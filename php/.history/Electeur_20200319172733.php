<?php
require "Personne";


class Electeur extends Personne
{
    private $bureauvote ;
    private $vote ;

    public function __construct($bureauvote,$vote,$nom,$prenom)
    {
        parent::__construct($nom,$prenom);
        $this->bureauvote = $bureauvote;
        $this->vote = $vote ;
    }

}